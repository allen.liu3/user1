package main

import (
	"fmt"

	"gitlab.com/allen.liu3/user1/info"
	"gitlab.com/allen.liu3/user2/data"
)

func main() {
	u1 := info.User1{
		Name: "u1",
	}

	u2 := data.User2{
		Name: "u1",
	}

	fmt.Println(u1)
	fmt.Println(u2)
}
